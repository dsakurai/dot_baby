#!/usr/bin/env bash

# clone submodules
(cd ~/.dot_baby && git submodule update --init --recursive .)

# zsh
mkdir  ~/.oh-my-zsh
curl -L https://github.com/ohmyzsh/ohmyzsh/tarball/master | tar -xz -C ~/.oh-my-zsh --strip-components 1
#sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
# back-up zsh initialization script
[ -e ~/.zshrc ] && mv ~/.zshrc ~/.zshrc-pre-dot_baby
# write the contents
echo "source ~/.dot_baby/zsh/zshrc.sh" > ~/.zshrc 
[ -e ~/.zshrc-pre-dot_baby ] && cat ~/.zshrc-pre-dot_baby > ~/.zshrc 

# tmux
ln -s ~/.dot_baby/tmux/tmux.conf ~/.tmux.conf 

