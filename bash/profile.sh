
if [ -z ${BABY_LOADED_BASH_PROFILE+x} ]; then
    # This file is not loaded yet
    BABY_LOADED_BASH_PROFILE=true
    # Set locale
    export LANG=en_US.UTF-8
    # For zsh to show unicode on input lines (output lines don't require this)
    export LC_ALL=en_US.UTF-8

    # Use the built-in ranger if not found on system
    export PATH="${PATH}:${HOME}/.dot_baby/ranger/local/bin"
fi

