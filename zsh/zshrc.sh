source "$HOME/.dot_baby/bash/profile.sh"

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="robbyrussell"

# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# COMPLETION SETTINGS
# add custom completion scripts
fpath+=("$HOME/.dot_baby/zsh/completion/cmake")

plugins+=(git docker pip)

source $ZSH/oh-my-zsh.sh

# Zsh options

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to  shown in the command execution time stamp 
# in the history command output. The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|
# yyyy-mm-dd
HIST_STAMPS="yyyy-mm-dd"
alias history="fc -li"

zstyle ':completion:*' auto-description 'argument for %d'
zstyle ':completion:*' completer _oldlist _expand _complete _ignored _match _correct
zstyle ':completion:*' completions 'NUMERIC == 2'
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' glob 1
zstyle ':completion:*' group-name ''
zstyle ':completion:*' match-original only
zstyle ':completion:*' max-errors 2 not-numeric
zstyle ':completion:*' prompt 'Typo? number of errors: %e'
zstyle ':completion:*' substitute 'NUMERIC == 3'
zstyle ':completion:*' verbose true
zstyle ':completion:*:zsh:*' verbose no
zstyle :compinstall filename "${HOME}/.zshrc"

autoload -Uz compinit
compinit

HISTFILE=~/.zsh_history
HISTSIZE=10000000000000
SAVEHIST=10000000000000

setopt HIST_IGNORE_SPACE # Remove command from history list when the first character is a space

# set command propmt
autoload -U colors && colors

setopt auto_cd
setopt auto_pushd # cd -TAB for listing visited directories
setopt correct # wrong command -> suggest correction
setopt list_packed # pack complete list to smaller region
setopt nolistbeep

#
# HISTORY
#
# Note that overriding zshaddhistory() results in ignoring some of these options easily.
#
setopt inc_append_history # save every command before it is executed (this is different from bash's history -a solution):
setopt share_history # retrieve the history file everytime history is called upon
# setopt HIST_SAVE_NO_DUPS # zsh would remove the duplicates
# Save each command's beginning timestamp (in seconds since the epoch) and the duration (in seconds) to the history fil
# The format of this prefixed data is: ‘: <beginning time>:<elapsed seconds>;<command>’
setopt EXTENDED_HISTORY # Save each command’s beginning timestamp (in seconds since the epoch) and the duration (in seconds) to the history file. The format of this prefixed data is: ‘: <beginning time>:<elapsed seconds>;<command>’
#setopt histignorealldups # avoid all duplicates
setopt histignorespace # prevents the current line from being saved if it begins with a space
setopt incappendhistory # Immediately append to the history

# basic completion
bindkey '^i' expand-or-complete # Tab key behavior
setopt COMPLETE_IN_WORD

# completion from history
zle -C hist-complete complete-word _generic
zstyle ':completion:hist-complete:*' completer _history
bindkey "^Y" hist-complete

# ignore .o files and ~ files
zstyle ':completion:*:*files' ignored-patterns '*?.o' '*?~'

# move cursor to end after completion
setopt ALWAYS_TO_END

setopt brace_ccl # mkdir {a-c} -> a b c


export LSCOLORS=exfxcxdxbxegedabagacad
export LS_COLORS='di=34:ln=35:so=32:pi=33:ex=31:bd=46;34:cd=43;34:su=41;30:sg=46;30:tw=42;30:ow=43;30'

alias grep='grep --line-number --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'

zstyle ':completion:*' list-colors ''
zstyle ':completion*' menu select=1 # enable arrow for completion even if there's few items

# handle utf-8 in less command
export LESSCHARSET=utf-8

bindkey 'OA' history-beginning-search-backward
bindkey 'OB' history-beginning-search-forward

PS1='[%m:${(%):-%~}]
$ '

