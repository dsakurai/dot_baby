# What is this project

Zsh:
- Type in a partial line, hit the up arrow key to find previous commands in your history 
- Hit tab in the shell and use arrow keys to complete files, variables, git commands, etc.

tmux:
- Interact using mouse
- tmux opens a new pane in the current directory

Miscellaneous:
- Use ranger

## Installation

Install dot_baby the easy way:
~~~
$ git clone https://gitlab.com/dsakurai/dot_baby.git ~/.dot_baby
$ ~/.dot_baby/details/setup/setup.sh
~~~

Change the login shell to zsh (this is optional):
~~~
$ usermod -s `which zsh` $USER
~~~
Your OS may require the root privilege to do this.

### Install dot_baby in a Vagrantfile

Add these lines:
~~~
    config.vm.provision "shell", inline: <<-SHELL
    
        sudo echo grub-pc hold | dpkg --set-selections # Don't upgrade GRUB because it requires manual intervention during upgrade...
        sudo apt-get update
        # and optionally
        # sudo apt-get full-upgrade -y # time-consuming

        #...

        sudo apt-get install zsh tmux git -y

        #...
    
        # login shell: zsh
        usermod -s /bin/zsh vagrant
        
    SHELL
    
        # Non-root commands
        config.vm.provision "shell", privileged: false, inline: <<-SHELL

        # Install dot_baby
        git clone https://gitlab.com/dsakurai/dot_baby.git /home/vagrant/.dot_baby
        /home/vagrant/.dot_baby/details/setup/setup.sh
    
    SHELL
~~~

You can test the system on the Vagrant virtual machine that comes with this project:
This is not recommended for endusers because the operation may pollute git configurations from the OS.
~~~
$ vagrant up
~~~
